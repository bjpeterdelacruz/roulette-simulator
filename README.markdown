# Roulette Simulator

## User Guide

**Note:** You **must** have Java Runtime Environment (JRE) version 7 installed to run the application. You
can download the JRE for your operating system
[here](http://www.oracle.com/technetwork/java/javase/downloads/java-se-jre-7-download-432155.html).

1. Open the Command Prompt on Windows or the Terminal on Mac OS X.
2. Type `java -jar simulator.jar`.

## Screenshots

![Main Screen](http://www.bjpeterdelacruz.com/files/roulette_main_screen.png Main screen showing results of using John Wayne's strategy)

![Custom Strategy](http://www.bjpeterdelacruz.com/files/roulette_custom.png Placing different kinds of bets)

![Running on Mac OS X](http://www.bjpeterdelacruz.com/files/roulette_macosx.png Running the simulator on Mac OS X 10.10)

## Developer Guide

1. Install the following:
    * Java SDK version 7
    * Gradle version 2.2

2. Run `gradle build` to compile and assemble an executable JAR file.

3. Run `gradle eclipse` to build an Eclipse project.

4. Run `gradle check` to execute Checkstyle, PMD, and FindBugs.

## Links
[Developer's Website](http://www.bjpeterdelacruz.com)
