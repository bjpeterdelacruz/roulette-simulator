package com.bpd.roulette;

import static org.junit.Assert.assertEquals;
import java.awt.Color;
import java.util.Arrays;
import org.junit.Test;

/**
 * This test suite tests the many different types of bets that can be made in roulette.
 * 
 * @author BJ Peter DeLaCruz (bj.peter.delacruz@gmail.com)
 */
public class RouletteTableTest {

  /**
   * Tests the Straight bets.
   */
  @Test
  public void testStraights() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 0; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(i), InsideBetType.STRAIGHT));
      int earnings = table.checkBets(i);
      assertEquals(35, earnings);
      table.clearBets();
    }

    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(RouletteNumber.DOUBLE_ZERO),
        InsideBetType.STRAIGHT));
    int earnings = table.checkBets(RouletteNumber.DOUBLE_ZERO);
    assertEquals(35, earnings);
    table.clearBets();
  }

  /**
   * Tests the Split bets.
   */
  @Test
  public void testSplits() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i < 34; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(i, i + 3), InsideBetType.SPLIT));
      int earnings = table.checkBets(i);
      assertEquals(17, earnings);
      table.clearBets();
    }

    for (int i = 2; i < 35; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(i, i + 3), InsideBetType.SPLIT));
      int earnings = table.checkBets(i);
      assertEquals(17, earnings);
      table.clearBets();
    }

    for (int i = 3; i < 36; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(i, i + 3), InsideBetType.SPLIT));
      int earnings = table.checkBets(i);
      assertEquals(17, earnings);
      table.clearBets();
    }

    for (int i = 1; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(i, i + 1), InsideBetType.SPLIT));
      int earnings = table.checkBets(i);
      assertEquals(17, earnings);
      table.clearBets();
    }

    for (int i = 2; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(i, i + 1), InsideBetType.SPLIT));
      int earnings = table.checkBets(i);
      assertEquals(17, earnings);
      table.clearBets();
    }

    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(0, RouletteNumber.DOUBLE_ZERO),
        InsideBetType.SPLIT));
    int earnings = table.checkBets(0);
    assertEquals(17, earnings);
    table.clearBets();

    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(0, RouletteNumber.DOUBLE_ZERO),
        InsideBetType.SPLIT));
    earnings = table.checkBets(RouletteNumber.DOUBLE_ZERO);
    assertEquals(17, earnings);
    table.clearBets();
  }

  /**
   * Tests the Street bets.
   */
  @Test
  public void testStreets() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 3; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(i), InsideBetType.STREET));
      int earnings = table.checkBets(i);
      assertEquals(11, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests the Corner bets.
   */
  @Test
  public void testCorners() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 5; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(i - 4, i - 3, i - 1, i),
          InsideBetType.CORNER));
      int earnings = table.checkBets(i - 1);
      assertEquals(8, earnings);
      table.clearBets();

      table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(i - 3, i - 2, i, i + 1),
          InsideBetType.CORNER));
      earnings = table.checkBets(i + 1);
      assertEquals(8, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests the Double Street bets.
   */
  @Test
  public void testDoubleStreets() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 6; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(
          Arrays.asList(i - 5, i - 4, i - 3, i - 2, i - 1, i), InsideBetType.DOUBLE_STREET));
      int earnings = table.checkBets(i);
      assertEquals(5, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests the Basket bet.
   */
  @Test
  public void testBasket() {
    RouletteTable table = new RouletteTable(100);
    Bet bet =
        RouletteUtils.createDollarBet(Arrays.asList(0, RouletteNumber.DOUBLE_ZERO, 1, 2, 3),
            InsideBetType.TOP_LINE);

    table.placeBets(bet);
    int earnings = table.checkBets(0);
    assertEquals(6, earnings);
    table.clearBets();

    table.placeBets(bet);
    earnings = table.checkBets(RouletteNumber.DOUBLE_ZERO);
    assertEquals(6, earnings);
    table.clearBets();

    table.placeBets(bet);
    earnings = table.checkBets(1);
    assertEquals(6, earnings);
    table.clearBets();

    table.placeBets(bet);
    earnings = table.checkBets(2);
    assertEquals(6, earnings);
    table.clearBets();

    table.placeBets(bet);
    earnings = table.checkBets(3);
    assertEquals(6, earnings);
    table.clearBets();
  }

  /**
   * Tests a Top Line bet (squares 0, 00, 1, 2, and 3).
   */
  @Test
  public void testTopLine() {
    RouletteTable table = new RouletteTable(100);
    Bet bet = RouletteUtils.createDollarBet(Arrays.asList(0, 1, 2), InsideBetType.BASKET);

    table.placeBets(bet);
    int earnings = table.checkBets(0);
    assertEquals(11, earnings);
    table.clearBets();

    table.placeBets(bet);
    earnings = table.checkBets(1);
    assertEquals(11, earnings);
    table.clearBets();

    table.placeBets(bet);
    earnings = table.checkBets(2);
    assertEquals(11, earnings);
    table.clearBets();

    bet =
        RouletteUtils.createDollarBet(Arrays.asList(0, RouletteNumber.DOUBLE_ZERO, 2),
            InsideBetType.BASKET);

    table.placeBets(bet);
    earnings = table.checkBets(0);
    assertEquals(11, earnings);
    table.clearBets();

    table.placeBets(bet);
    earnings = table.checkBets(RouletteNumber.DOUBLE_ZERO);
    assertEquals(11, earnings);
    table.clearBets();

    table.placeBets(bet);
    earnings = table.checkBets(2);
    assertEquals(11, earnings);
    table.clearBets();

    bet =
        RouletteUtils.createDollarBet(Arrays.asList(RouletteNumber.DOUBLE_ZERO, 2, 3),
            InsideBetType.BASKET);

    table.placeBets(bet);
    earnings = table.checkBets(RouletteNumber.DOUBLE_ZERO);
    assertEquals(11, earnings);
    table.clearBets();

    table.placeBets(bet);
    earnings = table.checkBets(2);
    assertEquals(11, earnings);
    table.clearBets();

    table.placeBets(bet);
    earnings = table.checkBets(3);
    assertEquals(11, earnings);
    table.clearBets();
  }

  /**
   * Tests a Dozen bet (squares 1 thru 12).
   */
  @Test
  public void testFirstDozen() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i <= 36; i++) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.FIRST_DOZEN));
      int earnings = table.checkBets(i);
      assertEquals(i < 13 ? 2 : -1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests a Dozen bet (squares 13 thru 24).
   */
  @Test
  public void testSecondDozen() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i <= 36; i++) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.SECOND_DOZEN));
      int earnings = table.checkBets(i);
      assertEquals(i > 12 && i < 25 ? 2 : -1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests a Dozen bet (squares 25 thru 36.)
   */
  @Test
  public void testThirdDozen() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i <= 36; i++) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.THIRD_DOZEN));
      int earnings = table.checkBets(i);
      assertEquals(i > 24 ? 2 : -1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests a Column bet (1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34).
   */
  @Test
  public void testFirstColumn() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.FIRST_COLUMN));
      int earnings = table.checkBets(i);
      assertEquals(2, earnings);
      table.clearBets();
    }

    for (int i = 2; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.FIRST_COLUMN));
      int earnings = table.checkBets(i);
      assertEquals(-1, earnings);
      table.clearBets();
    }

    for (int i = 3; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.FIRST_COLUMN));
      int earnings = table.checkBets(i);
      assertEquals(-1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests a Column bet (2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35).
   */
  @Test
  public void testSecondColumn() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.SECOND_COLUMN));
      int earnings = table.checkBets(i);
      assertEquals(-1, earnings);
      table.clearBets();
    }

    for (int i = 2; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.SECOND_COLUMN));
      int earnings = table.checkBets(i);
      assertEquals(2, earnings);
      table.clearBets();
    }

    for (int i = 3; i < 37; i += 3) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.SECOND_COLUMN));
      int earnings = table.checkBets(i);
      assertEquals(-1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests a Column bet (3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36).
   */
  @Test
  public void testThirdColumn() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i <= 36; i++) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.THIRD_COLUMN));
      int earnings = table.checkBets(i);
      assertEquals(i % 3 == 0 ? 2 : -1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests the Manque bet (squares 1 thru 18).
   */
  @Test
  public void testFirstEighteen() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i <= 36; i++) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.FIRST_EIGHTEEN));
      int earnings = table.checkBets(i);
      assertEquals(i < 19 ? 1 : -1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests the Passe bet (squares 19 thru 36).
   */
  @Test
  public void testLastEighteen() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i <= 36; i++) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.LAST_EIGHTEEN));
      int earnings = table.checkBets(i);
      assertEquals(i < 19 ? -1 : 1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests the Even bets.
   */
  @Test
  public void testEvens() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i <= 36; i++) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.EVEN));
      int earnings = table.checkBets(i);
      assertEquals(i % 2 == 0 ? 1 : -1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests the Odd bets.
   */
  @Test
  public void testOdds() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i <= 36; i++) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.ODD));
      int earnings = table.checkBets(i);
      assertEquals(i % 2 == 0 ? -1 : 1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests the Black bets.
   */
  @Test
  public void testBlacks() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i <= 36; i++) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.BLACK));
      int earnings = table.checkBets(i);
      RouletteNumber rn = RouletteNumber.createNumber(i);
      assertEquals(rn.getColor().equals(Color.BLACK) ? 1 : -1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests the Red bets.
   */
  @Test
  public void testReds() {
    RouletteTable table = new RouletteTable(100);
    for (int i = 1; i <= 36; i++) {
      table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.RED));
      int earnings = table.checkBets(i);
      RouletteNumber rn = RouletteNumber.createNumber(i);
      assertEquals(rn.getColor().equals(Color.RED) ? 1 : -1, earnings);
      table.clearBets();
    }
  }

  /**
   * Tests whether the amount won or lost is correct.
   */
  @Test
  public void testAmountCalculation() {
    RouletteTable table = new RouletteTable(100);
    table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.RED));
    table.checkBets(2);
    assertEquals(99, table.getBankroll());

    table = new RouletteTable(100);
    table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.RED));
    table.checkBets(1);
    assertEquals(101, table.getBankroll());
  }

  /**
   * Tests whether a bet can be placed if no money is left in the bankroll.
   */
  @Test(expected = IllegalStateException.class)
  public void testBankroll() {
    RouletteTable table = new RouletteTable(1);
    table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.RED));
    table.placeBets(RouletteUtils.createDollarBet(OutsideBetType.EVEN));
  }

}
