package com.bpd.roulette.simulators;

import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import org.junit.Test;
import com.bpd.roulette.InsideBetType;
import com.bpd.roulette.OutsideBetType;
import com.bpd.roulette.RouletteNumber;
import com.bpd.roulette.RouletteTable;
import com.bpd.roulette.RouletteUtils;

/**
 * This test suite tests the Red Split Black strategy.
 * 
 * @author BJ Peter DeLaCruz (bj.peter.delacruz@gmail.com)
 */
public class RedSplitBlackSimulatorTest {

  /**
   * Tests the Red Split Black strategy.
   */
  @Test
  public void testStrategy() {
    RouletteTable table = new RouletteTable(500);
    table.placeBets(RouletteUtils.createOutsideBet(7, OutsideBetType.RED));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(8, 11), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(17, 20), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(26, 29), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(10, 13), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(28, 31), InsideBetType.SPLIT));
    table.checkBets(RouletteNumber.createNumber(1));
    assertEquals(502, table.getBankroll());
    table.clearBets();

    table = new RouletteTable(500);
    table.placeBets(RouletteUtils.createOutsideBet(7, OutsideBetType.RED));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(8, 11), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(17, 20), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(26, 29), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(10, 13), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(28, 31), InsideBetType.SPLIT));
    table.checkBets(RouletteNumber.createNumber(11));
    assertEquals(506, table.getBankroll());
    table.clearBets();

    table = new RouletteTable(500);
    table.placeBets(RouletteUtils.createOutsideBet(7, OutsideBetType.RED));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(8, 11), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(17, 20), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(26, 29), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(10, 13), InsideBetType.SPLIT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(28, 31), InsideBetType.SPLIT));
    table.checkBets(RouletteNumber.createNumber(2));
    assertEquals(488, table.getBankroll());
    table.clearBets();
  }

}
