package com.bpd.roulette.simulators;

import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import org.junit.Test;
import com.bpd.roulette.InsideBetType;
import com.bpd.roulette.RouletteNumber;
import com.bpd.roulette.RouletteTable;
import com.bpd.roulette.RouletteUtils;

/**
 * This test suite tests the "John Wayne" strategy.
 * 
 * @author BJ Peter DeLaCruz (bj.peter.delacruz@gmail.com)
 */
public class JohnWayneStrategySimulatorTest {

  /**
   * Tests the "John Wayne" strategy.
   */
  @Test
  public void testStrategy() {
    RouletteTable table = new RouletteTable(500);
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(8), InsideBetType.STRAIGHT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(4, 5, 7, 8), InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(5, 6, 8, 9), InsideBetType.CORNER));
    table
        .placeBets(RouletteUtils.createDollarBet(Arrays.asList(7, 8, 10, 11),
            InsideBetType.CORNER));
    table
        .placeBets(RouletteUtils.createDollarBet(Arrays.asList(8, 9, 11, 12),
            InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(20), InsideBetType.STRAIGHT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(16, 17, 19, 20),
        InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(17, 18, 20, 21),
        InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(19, 20, 22, 23),
        InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(20, 21, 23, 24),
        InsideBetType.CORNER));
    table.checkBets(RouletteNumber.createNumber(8));
    assertEquals(562, table.getBankroll());
    table.clearBets();

    table = new RouletteTable(500);
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(8), InsideBetType.STRAIGHT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(4, 5, 7, 8), InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(5, 6, 8, 9), InsideBetType.CORNER));
    table
        .placeBets(RouletteUtils.createDollarBet(Arrays.asList(7, 8, 10, 11),
            InsideBetType.CORNER));
    table
        .placeBets(RouletteUtils.createDollarBet(Arrays.asList(8, 9, 11, 12),
            InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(20), InsideBetType.STRAIGHT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(16, 17, 19, 20),
        InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(17, 18, 20, 21),
        InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(19, 20, 22, 23),
        InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(20, 21, 23, 24),
        InsideBetType.CORNER));
    table.checkBets(RouletteNumber.createNumber(5));
    assertEquals(508, table.getBankroll());
    table.clearBets();

    table = new RouletteTable(500);
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(8), InsideBetType.STRAIGHT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(4, 5, 7, 8), InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(5, 6, 8, 9), InsideBetType.CORNER));
    table
        .placeBets(RouletteUtils.createDollarBet(Arrays.asList(7, 8, 10, 11),
            InsideBetType.CORNER));
    table
        .placeBets(RouletteUtils.createDollarBet(Arrays.asList(8, 9, 11, 12),
            InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(20), InsideBetType.STRAIGHT));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(16, 17, 19, 20),
        InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(17, 18, 20, 21),
        InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(19, 20, 22, 23),
        InsideBetType.CORNER));
    table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(20, 21, 23, 24),
        InsideBetType.CORNER));
    table.checkBets(RouletteNumber.createNumber(4));
    assertEquals(499, table.getBankroll());
    table.clearBets();
  }

}
