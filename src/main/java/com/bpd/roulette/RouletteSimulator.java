package com.bpd.roulette;

/**
 * An interface for implementing different roulette betting strategies.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public interface RouletteSimulator {

  /** Runs the simulator. */
  void run();

  /**
   * @return The results of the simulation. Throws an exception if the simulation hasn't been run
   * yet.
   */
  String getResults();

  /**
   * Returns the name of this simulation (a.k.a. betting strategy).
   * 
   * @return The name of this simulation.
   */
  String getName();

}
