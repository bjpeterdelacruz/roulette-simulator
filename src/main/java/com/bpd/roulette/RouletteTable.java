package com.bpd.roulette;

import static com.bpd.roulette.RouletteNumber.createNumber;
import static com.bpd.roulette.RouletteNumber.createNumbers;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Represents a roulette table. Contains all of the possible combinations of bets that can be made
 * by a player.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public final class RouletteTable {

  // Inside bets
  private static final List<RouletteNumber> STRAIGHTS;
  private static final List<List<RouletteNumber>> SPLITS = getAllSplits();
  private static final List<List<RouletteNumber>> CORNERS = getAllCorners();
  private static final List<List<RouletteNumber>> STREETS = getAllStreets();
  private static final List<List<RouletteNumber>> DOUBLE_STREETS = getAllDoubleStreets();
  private static final List<List<RouletteNumber>> BASKET;
  private static final List<RouletteNumber> TOP_LINE;

  static {
    STRAIGHTS = new ArrayList<>();
    for (int i = 0; i < 37; i++) {
      STRAIGHTS.add(createNumber(i));
    }
    STRAIGHTS.add(createNumber(RouletteNumber.DOUBLE_ZERO));

    BASKET = new ArrayList<>();
    BASKET.add(createNumbers(0, 1, 2));
    BASKET.add(createNumbers(0, RouletteNumber.DOUBLE_ZERO, 2));
    BASKET.add(createNumbers(RouletteNumber.DOUBLE_ZERO, 2, 3));

    TOP_LINE = createNumbers(0, RouletteNumber.DOUBLE_ZERO, 1, 2, 3);
  }

  // Outside bets
  private static final List<RouletteNumber> FIRST_DOZEN = getDozen(1, 12);
  private static final List<RouletteNumber> SECOND_DOZEN = getDozen(13, 24);
  private static final List<RouletteNumber> THIRD_DOZEN = getDozen(25, 36);

  private static final List<RouletteNumber> FIRST_COLUMN = getColumn(1);
  private static final List<RouletteNumber> SECOND_COLUMN = getColumn(2);
  private static final List<RouletteNumber> THIRD_COLUMN = getColumn(3);

  private static final List<RouletteNumber> FIRST_EIGHTEEN;
  private static final List<RouletteNumber> LAST_EIGHTEEN;
  private static final List<RouletteNumber> EVENS;
  private static final List<RouletteNumber> ODDS;

  static {
    EVENS = new ArrayList<>();
    ODDS = new ArrayList<>();

    FIRST_EIGHTEEN = new ArrayList<>();
    for (int i = 1; i <= 18; i++) {
      RouletteNumber number = createNumber(i);
      FIRST_EIGHTEEN.add(number);
      if (i % 2 == 0) {
        EVENS.add(number);
      }
      else {
        ODDS.add(number);
      }
    }

    LAST_EIGHTEEN = new ArrayList<>();
    for (int i = 19; i <= 36; i++) {
      RouletteNumber number = createNumber(i);
      LAST_EIGHTEEN.add(number);
      if (i % 2 == 0) {
        EVENS.add(number);
      }
      else {
        ODDS.add(number);
      }
    }
  }

  private int bankroll;
  private int amount;
  private final List<Bet> bets;

  /**
   * Creates a new RouletteTable.
   * 
   * @param bankroll The amount in U.S. dollars used for betting on this table.
   */
  public RouletteTable(int bankroll) {
    if (bankroll <= 0) {
      throw new IllegalArgumentException("bankroll must be 1 or greater.");
    }
    bets = new ArrayList<>();
    this.bankroll = bankroll;
    this.amount = bankroll;
  }

  /**
   * Places the given bets on the table. Throws an exception if the wager for a bet is greater
   * than the current bankroll.
   * 
   * @param bets The bets to place on the table.
   */
  public void placeBets(Bet... bets) {
    if (bets == null) {
      throw new IllegalArgumentException("bets is null.");
    }
    for (Bet bet : bets) {
      amount -= bet.getWager();
      if (amount < 0) {
        throw new IllegalStateException("Not enough money left for bet.");
      }
    }
    this.bets.addAll(Arrays.asList(bets));
  }

  /** Clears the bets that are currently on the table. */
  public void clearBets() {
    bets.clear();
  }

  /** @return The amount in U.S. dollars used for betting on this table. */
  public int getBankroll() {
    return bankroll;
  }

  /**
   * Checks the bets using the given winning number to see if a player has lost or won bets.
   * 
   * @param winningNumber The winning number.
   */
  public void checkBets(RouletteNumber winningNumber) {
    checkBets(winningNumber.getValue());
  }

  /**
   * Checks the bets using the given winning number to see if a player has lost or won bets.
   * 
   * @param winningNumber The winning number.
   * @return The net amount in U.S. dollars won or lost.
   */
  int checkBets(int winningNumber) {
    RouletteNumber number = createNumber(winningNumber);
    int earnings = 0;
    for (Bet bet : bets) {
      if (bet.getType() instanceof InsideBetType) {
        InsideBetType type = (InsideBetType) bet.getType();
        switch (type) {
        case STRAIGHT:
          boolean isNullOrEmpty = bet.getNumbers() == null || bet.getNumbers().isEmpty();
          if (isNullOrEmpty || bet.getNumbers().size() > 1) {
            throw new IllegalStateException("Only one bet should have been made.");
          }
          if (number.equals(bet.getNumbers().get(0))) {
            earnings += bet.getWager() * type.getPayout();
          }
          else {
            earnings -= bet.getWager();
          }
          break;
        case SPLIT:
          for (List<RouletteNumber> numbers : SPLITS) {
            earnings += calculateEarnings(number, bet, numbers);
          }
          break;
        case STREET:
          for (List<RouletteNumber> numbers : STREETS) {
            earnings += calculateEarnings(number, bet, numbers);
          }
          break;
        case CORNER:
          for (List<RouletteNumber> numbers : CORNERS) {
            earnings += calculateEarnings(number, bet, numbers);
          }
          break;
        case DOUBLE_STREET:
          for (List<RouletteNumber> numbers : DOUBLE_STREETS) {
            earnings += calculateEarnings(number, bet, numbers);
          }
          break;
        case BASKET:
          for (List<RouletteNumber> numbers : BASKET) {
            earnings += calculateEarnings(number, bet, numbers);
          }
          break;
        case TOP_LINE:
          earnings += calculateEarnings(number, bet, TOP_LINE);
          break;
        default:
          throw new IllegalArgumentException("Invalid bet type: " + type);
        }
      }
      else if (bet.getType() instanceof OutsideBetType) {
        OutsideBetType type = (OutsideBetType) bet.getType();
        switch (type) {
        case FIRST_DOZEN:
          earnings += calculateEarnings(number, bet, FIRST_DOZEN);
          break;
        case SECOND_DOZEN:
          earnings += calculateEarnings(number, bet, SECOND_DOZEN);
          break;
        case THIRD_DOZEN:
          earnings += calculateEarnings(number, bet, THIRD_DOZEN);
          break;
        case FIRST_COLUMN:
          earnings += calculateEarnings(number, bet, FIRST_COLUMN);
          break;
        case SECOND_COLUMN:
          earnings += calculateEarnings(number, bet, SECOND_COLUMN);
          break;
        case THIRD_COLUMN:
          earnings += calculateEarnings(number, bet, THIRD_COLUMN);
          break;
        case FIRST_EIGHTEEN:
          earnings += calculateEarnings(number, bet, FIRST_EIGHTEEN);
          break;
        case LAST_EIGHTEEN:
          earnings += calculateEarnings(number, bet, LAST_EIGHTEEN);
          break;
        case EVEN:
          earnings += calculateEarnings(number, bet, EVENS);
          break;
        case ODD:
          earnings += calculateEarnings(number, bet, ODDS);
          break;
        case BLACK:
          earnings += calculateEarnings(number, bet, RouletteNumber.BLACKS);
          break;
        case RED:
          earnings += calculateEarnings(number, bet, RouletteNumber.REDS);
          break;
        default:
          throw new IllegalArgumentException("Invalid bet type: " + type);
        }
      }
      else {
        throw new IllegalArgumentException("Invalid bet type: " + bet.getType());
      }
    }
    bankroll += earnings;
    amount = bankroll;
    return earnings;
  }

  /**
   * Calculates the amount in U.S. dollars won or lost for the given bet using the given winning
   * number and list of numbers, which may contain the winning number.
   * 
   * @param winningNumber The winning number.
   * @param bet The bet that contains the player's numbers.
   * @param numbers The list of numbers that may contain the winning number.
   * @return The amount in U.S. dollars won or lost (negative numbers represent money lost).
   */
  private static int calculateEarnings(RouletteNumber winningNumber, Bet bet,
      Collection<RouletteNumber> numbers) {
    if (winningNumber == null) {
      throw new IllegalArgumentException("winningNumber is null.");
    }
    if (bet == null) {
      throw new IllegalArgumentException("bet is null.");
    }
    if (numbers == null) {
      throw new IllegalArgumentException("numbers is null.");
    }
    if (bet.getType() instanceof OutsideBetType) {
      if (numbers.contains(winningNumber)) {
        return bet.getWager() * bet.getType().getPayout();
      }
      else {
        return bet.getWager() * -1;
      }
    }
    if (!numbers.contains(winningNumber) && !numbers.containsAll(bet.getNumbers())) {
      return 0;
    }
    if (!numbers.contains(winningNumber) && numbers.containsAll(bet.getNumbers())) {
      return bet.getWager() * -1;
    }
    if (numbers.contains(winningNumber) && numbers.containsAll(bet.getNumbers())) {
      return bet.getWager() * bet.getType().getPayout();
    }
    return 0;
  }

  /** @return All of the possible split bets (e.g. 1-2). */
  private static List<List<RouletteNumber>> getAllSplits() {
    List<List<RouletteNumber>> splits = new ArrayList<>();
    for (int i = 3; i < 37; i += 3) {
      splits.add(createNumbers(i - 2, i - 1));
      splits.add(createNumbers(i - 1, i));
    }
    for (int i = 6; i < 37; i += 3) {
      splits.add(createNumbers(i - 5, i - 2));
      splits.add(createNumbers(i - 4, i - 1));
      splits.add(createNumbers(i - 3, i));
    }
    splits.add(createNumbers(0, RouletteNumber.DOUBLE_ZERO));
    return splits;
  }

  /** @return All of the possible corner bets (e.g. 1-2-4-5). */
  private static List<List<RouletteNumber>> getAllCorners() {
    List<List<RouletteNumber>> corners = new ArrayList<>();
    for (int i = 5; i < 37; i += 3) {
      corners.add(createNumbers(i - 4, i - 3, i - 1, i));
      corners.add(createNumbers(i - 3, i - 2, i, i + 1));
    }
    return corners;
  }

  /** @return All of the possible street bets (e.g. 1-2-3). */
  private static List<List<RouletteNumber>> getAllStreets() {
    List<List<RouletteNumber>> streets = new ArrayList<>();
    for (int i = 3; i < 37; i += 3) {
      streets.add(createNumbers(i - 2, i - 1, i));
    }
    return streets;
  }

  /** @return All of the possible double street bets (e.g. 1-2-3-4-5-6). */
  private static List<List<RouletteNumber>> getAllDoubleStreets() {
    List<List<RouletteNumber>> streets = new ArrayList<>();
    for (int i = 6; i < 37; i += 3) {
      streets.add(createNumbers(i - 5, i - 4, i - 3, i - 2, i - 1, i));
    }
    return streets;
  }

  /**
   * Creates a list of a dozen numbers that contains the first and last numbers, inclusive.
   * 
   * @param start The first number in the dozen.
   * @param end The last number in the dozen.
   * @return All of the possible dozen bets for the given interval.
   */
  private static List<RouletteNumber> getDozen(int start, int end) {
    List<RouletteNumber> numbers = new ArrayList<>();
    for (int i = start; i <= end; i++) {
      numbers.add(createNumber(i));
    }
    if (numbers.size() != 12) {
      throw new IllegalArgumentException("Expected 12 numbers in the list.");
    }
    return numbers;
  }

  /**
   * Creates a list of twelve numbers that are found in the column that contains the given number.
   * 
   * @param start The first number in the column.
   * @return All of the possible column bets for a column, which starts with the given number.
   */
  private static List<RouletteNumber> getColumn(int start) {
    List<RouletteNumber> numbers = new ArrayList<>();
    for (int i = start; i < 37; i += 3) {
      numbers.add(createNumber(i));
    }
    if (numbers.size() != 12) {
      throw new IllegalArgumentException("Expected 12 numbers in the list.");
    }
    return numbers;
  }

}
