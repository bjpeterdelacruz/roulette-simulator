package com.bpd.roulette;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Represents a number on the roulette table.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public final class RouletteNumber {

  /** Represents a 00 in American roulette. */
  public static final int DOUBLE_ZERO = 37;
  /** Contains all black numbers. */
  public static final Set<RouletteNumber> BLACKS = getAllBlackNumbers();
  /** Contains all red numbers. */
  public static final Set<RouletteNumber> REDS = getAllRedNumbers();
  /** Contains 0 and 00. */
  public static final Set<RouletteNumber> ZEROES = getZeroes();

  private final Color color;
  private final int value;

  /**
   * Creates a new RouletteNumber.
   * 
   * @param color The color associated with this number.
   * @param value The value of this number.
   */
  private RouletteNumber(Color color, int value) {
    if (color == null) {
      throw new IllegalArgumentException("color is null.");
    }
    this.color = color;
    this.value = value;
  }

  /** @return The color associated with this number. */
  public Color getColor() {
    return color;
  }

  /** @return The value of this number. */
  public int getValue() {
    return value;
  }

  /** @return A human-readable display string representing this number. */
  public String getDisplayString() {
    if (color.equals(Color.RED)) {
      return value + (value < 10 ? " " : "") + " (RED)  ";
    }
    if (color.equals(Color.BLACK)) {
      return value + (value < 10 ? " " : "") + " (BLACK)";
    }
    if (value == RouletteNumber.DOUBLE_ZERO) {
      return "00        ";
    }
    return "0         ";
  }

  /**
   * Creates a RouletteNumber with the given value. Value must be 0, 00, or between 1 and 36,
   * inclusive.
   * 
   * @param value The value.
   * @return A RouletteNumber with the given value.
   */
  public static RouletteNumber createNumber(int value) {
    RouletteNumber rn = createRedNumber(value);
    if (REDS.contains(rn)) {
      return rn;
    }
    rn = createBlackNumber(value);
    if (BLACKS.contains(rn)) {
      return rn;
    }
    if (value == 0) {
      return createZero();
    }
    else if (value == DOUBLE_ZERO) {
      return createDoubleZero();
    }
    else {
      throw new IllegalArgumentException("Invalid value: " + value);
    }
  }

  /**
   * Creates a list of RouletteNumber objects with the given values.
   * 
   * @param values The values.
   * @return A list of RouletteNumber objects.
   */
  public static List<RouletteNumber> createNumbers(List<Integer> values) {
    List<RouletteNumber> numbers = new ArrayList<>();
    for (int value : values) {
      numbers.add(createNumber(value));
    }
    return numbers;
  }

  /**
   * Creates a list of RouletteNumber objects with the given values.
   * 
   * @param values The values.
   * @return A list of RouletteNumber objects.
   */
  public static List<RouletteNumber> createNumbers(int... values) {
    List<RouletteNumber> numbers = new ArrayList<>();
    for (int value : values) {
      numbers.add(createNumber(value));
    }
    return numbers;
  }

  /**
   * Creates a red RouletteNumber with the given value.
   * 
   * @param value The value.
   * @return A red RouletteNumber.
   */
  private static RouletteNumber createRedNumber(int value) {
    return new RouletteNumber(Color.RED, value);
  }

  /**
   * Creates a black RouletteNumber with the given value.
   * 
   * @param value The value.
   * @return A black RouletteNumber.
   */
  private static RouletteNumber createBlackNumber(int value) {
    return new RouletteNumber(Color.BLACK, value);
  }

  /** @return A RouletteNumber with the value 0. */
  private static RouletteNumber createZero() {
    return new RouletteNumber(Color.GREEN, 0);
  }

  /** @return A RouletteNumber with the value 00. */
  private static RouletteNumber createDoubleZero() {
    return new RouletteNumber(Color.GREEN, DOUBLE_ZERO);
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return Objects.hash(color, value);
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof RouletteNumber)) {
      return false;
    }
    RouletteNumber rn = (RouletteNumber) object;
    return rn.color.equals(color) && rn.value == value;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    String colorName;
    if (color.equals(Color.RED)) {
      colorName = "RED";
    }
    else if (color.equals(Color.BLACK)) {
      colorName = "BLACK";
    }
    else {
      colorName = "GREEN";
    }
    return "RouletteNumber[color=" + colorName + ", value=" + (value == DOUBLE_ZERO ? "00" : value)
        + "]";
  }

  /** @return A set that contains the numbers 0 and 00. */
  private static Set<RouletteNumber> getZeroes() {
    Set<RouletteNumber> zeroes = new HashSet<>();
    zeroes.add(createZero());
    zeroes.add(createDoubleZero());
    return zeroes;
  }

  /** @return A set that contains all red numbers. */
  private static Set<RouletteNumber> getAllRedNumbers() {
    Set<RouletteNumber> reds = new HashSet<>();
    reds.add(createRedNumber(1));
    reds.add(createRedNumber(3));
    reds.add(createRedNumber(5));
    reds.add(createRedNumber(7));
    reds.add(createRedNumber(9));
    reds.add(createRedNumber(12));
    reds.add(createRedNumber(14));
    reds.add(createRedNumber(16));
    reds.add(createRedNumber(18));
    reds.add(createRedNumber(19));
    reds.add(createRedNumber(21));
    reds.add(createRedNumber(23));
    reds.add(createRedNumber(25));
    reds.add(createRedNumber(27));
    reds.add(createRedNumber(30));
    reds.add(createRedNumber(32));
    reds.add(createRedNumber(34));
    reds.add(createRedNumber(36));
    return reds;
  }

  /** @return A set that contains all black numbers. */
  private static Set<RouletteNumber> getAllBlackNumbers() {
    Set<RouletteNumber> blacks = new HashSet<>();
    blacks.add(createBlackNumber(2));
    blacks.add(createBlackNumber(4));
    blacks.add(createBlackNumber(6));
    blacks.add(createBlackNumber(8));
    blacks.add(createBlackNumber(10));
    blacks.add(createBlackNumber(11));
    blacks.add(createBlackNumber(13));
    blacks.add(createBlackNumber(15));
    blacks.add(createBlackNumber(17));
    blacks.add(createBlackNumber(20));
    blacks.add(createBlackNumber(22));
    blacks.add(createBlackNumber(24));
    blacks.add(createBlackNumber(26));
    blacks.add(createBlackNumber(28));
    blacks.add(createBlackNumber(29));
    blacks.add(createBlackNumber(31));
    blacks.add(createBlackNumber(33));
    blacks.add(createBlackNumber(35));
    return blacks;
  }

}
