package com.bpd.roulette.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import net.miginfocom.swing.MigLayout;
import com.bpd.roulette.Bet;
import com.bpd.roulette.InsideBetType;
import com.bpd.roulette.OutsideBetType;
import com.bpd.roulette.RouletteNumber;
import com.bpd.roulette.RouletteUtils;
import com.bpd.roulette.main.Roulette;

/**
 * A panel in which a player can add or remove different types of bets.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public final class CreateCustomStrategyDialog extends JDialog {

  private static final long serialVersionUID = 1L;

  private static final Font FONT = new Font("Verdana", Font.PLAIN, 12);

  private final JComboBox<String> insideBetCombo = new JComboBox<>();
  private final JComboBox<String> outsideBetCombo = new JComboBox<>();

  private final JRadioButton insideBetBtn = new JRadioButton("Inside Bet");
  private final JRadioButton outsideBetBtn = new JRadioButton("Outside Bet");

  private final DefaultListModel<Bet> listModel = new DefaultListModel<>();
  private final JList<Bet> list = new JList<>(listModel);

  private final JTextField betsTextField = new JTextField();
  private final JTextField wagerTextField = new JTextField();

  private final JButton addBetBtn = new JButton("Add Bet");
  private final JMenuItem clearBetsMenuItem = new JMenuItem("Clear All Bets");

  /**
   * Creates a new instance of AddBetDialog.
   * 
   * @param parent The Roulette frame.
   * @param bets The list of bets that the user already created, may be null.
   */
  public CreateCustomStrategyDialog(final Roulette parent, List<Bet> bets) {
    if (parent == null) {
      throw new IllegalArgumentException("parent is null");
    }

    if (bets != null) {
      for (Bet bet : bets) {
        listModel.addElement(bet);
      }
    }

    URL iconUrl = getClass().getResource("images/roulette.png");
    if (iconUrl != null) {
      setIconImage(new ImageIcon(iconUrl).getImage());
    }

    setModalityType(ModalityType.APPLICATION_MODAL);

    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent event) {
        List<Bet> allBets = getAllBets();
        parent.setBets(allBets);
        parent.enableRunButton(!allBets.isEmpty());
      }

    });

    JPanel insideBetPanel = createInsideBetPanel();
    JPanel outsideBetPanel = createOutsideBetPanel();
    JPanel wagerPanel = createWagerPanel();

    insideBetBtn.setPreferredSize(outsideBetBtn.getPreferredSize());
    outsideBetCombo.setPreferredSize(insideBetCombo.getPreferredSize());

    insideBetBtn.setFont(FONT);
    outsideBetBtn.setFont(FONT);

    insideBetCombo.setFont(FONT);
    outsideBetCombo.setFont(FONT);

    ButtonGroup group = new ButtonGroup();
    group.add(outsideBetBtn);
    group.add(insideBetBtn);

    JPanel leftPanel = new JPanel(new MigLayout());
    leftPanel.add(insideBetPanel, "wrap");
    leftPanel.add(outsideBetPanel, "wrap");
    leftPanel.add(createBetsPanel(), "wrap, grow");
    leftPanel.add(wagerPanel, "wrap");
    leftPanel.add(createBetsList(), "grow, hmin 275, wmin 220");

    JPanel buttonsPanel = new JPanel(new BorderLayout());

    setupAddBetsButton();

    addBetBtn.setEnabled(false);
    addBetBtn.setFont(FONT);

    final JButton doneButton = new JButton("Done");
    doneButton.setFont(FONT);
    doneButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        WindowEvent windowClosing =
            new WindowEvent(CreateCustomStrategyDialog.this, WindowEvent.WINDOW_CLOSING);
        dispatchEvent(windowClosing);
      }

    });

    JPanel right = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    right.add(addBetBtn);
    right.add(doneButton);

    doneButton.setPreferredSize(addBetBtn.getPreferredSize());

    buttonsPanel.add(right, BorderLayout.EAST);

    JMenuBar menuBar = new JMenuBar();
    JMenu optionsMenu = new JMenu("Options");
    clearBetsMenuItem.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        listModel.removeAllElements();
        betsTextField.setText("");
        wagerTextField.setText("");
        clearBetsMenuItem.setEnabled(false);
        addBetBtn.setEnabled(false);
      }

    });
    clearBetsMenuItem.setEnabled(!listModel.isEmpty());
    optionsMenu.add(clearBetsMenuItem);
    menuBar.add(optionsMenu);
    setJMenuBar(menuBar);

    setTitle("Create Your Own Strategy");
    setLayout(new MigLayout(""));
    add(leftPanel, "grow");
    add(buttonsPanel, "south");
    insideBetBtn.doClick();
    outsideBetCombo.setPreferredSize(insideBetCombo.getPreferredSize());
  }

  /**
   * Returns a panel that contains a text field in which the user can enter bets, from 0 to 36,
   * including 00.
   * 
   * @return A panel that contains a text field that only accepts a comma-separated list of numbers.
   */
  private JPanel createBetsPanel() {
    JPanel betsPanel = new JPanel(new MigLayout("", "0[grow,fill]0", "0[]5[]0"));
    String text =
        "<html>For single bets, separate each number with a comma.<br><br>"
            + "For split, street, corner, double street, and basket bets, separate<br>"
            + "each number with a hyphen and each series with a comma.<br><br>"
            + "Examples: 0,00,10,25,36 and 14-7 and 0-00-2,0-1-2</html>";
    JLabel question = new JLabel(text);
    question.setFont(FONT);
    betsPanel.add(question, "wrap");

    betsTextField.setFont(FONT);
    betsTextField.setDocument(new NumberDocument2());
    betsTextField.getDocument().addDocumentListener(new TextFieldListener());

    betsPanel.add(betsTextField, "grow");

    return betsPanel;
  }

  /**
   * Sets up the Add Bet button (adds an action listener, etc.).
   */
  private void setupAddBetsButton() {
    addBetBtn.addActionListener(new ActionListener() {

      private boolean updateListModel(List<Bet> bets) {
        if (bets == null || bets.isEmpty()) {
          return false;
        }

        Bet selectedBet = list.getSelectedValue();
        List<Bet> allBets = new ArrayList<>();
        for (int index = 0; index < listModel.getSize(); index++) {
          allBets.add(listModel.get(index));
        }
        allBets.addAll(bets);
        allBets = new ArrayList<>(new HashSet<>(allBets));
        Collections.sort(allBets, new Comparator<Bet>() {

          @Override
          public int compare(Bet bet1, Bet bet2) {
            int result = bet1.getType().getDisplayName().compareTo(bet2.getType().getDisplayName());
            if (result != 0) {
              return result;
            }
            result = Integer.compare(bet1.getWager(), bet2.getWager());
            if (result != 0) {
              return result;
            }
            return Integer.compare(bet1.getNumbers().size(), bet2.getNumbers().size());
          }

        });
        listModel.clear();
        for (Bet bet : allBets) {
          listModel.addElement(bet);
        }
        if (selectedBet != null) {
          list.setSelectedValue(selectedBet, true);
        }

        betsTextField.setText("");
        wagerTextField.setText("");
        return true;
      }

      @Override
      public void actionPerformed(ActionEvent event) {
        int wager = Integer.parseInt(wagerTextField.getText());
        boolean isListModelUpdated = false;
        if (outsideBetBtn.isSelected()) {
          String selectedBetType = outsideBetCombo.getSelectedItem().toString();
          OutsideBetType type = OutsideBetType.getEnumFromDisplayName(selectedBetType);
          Bet bet = RouletteUtils.createOutsideBet(wager, type);
          isListModelUpdated = updateListModel(Arrays.asList(bet));
        }
        else if (insideBetBtn.isSelected()) {
          String item = insideBetCombo.getSelectedItem().toString();

          List<Bet> bets = new ArrayList<>();

          InsideBetType type = InsideBetType.getEnumFromDisplayName(item);

          List<String> numbers = Arrays.asList(betsTextField.getText().split(","));
          List<String> temp1 = new ArrayList<>();
          for (String number : numbers) {
            String errorMsg = "Invalid " + type.getDisplayName().toLowerCase() + " bet: " + number;
            if (type == InsideBetType.TOP_LINE) {
              break;
            }
            if ("00".equals(number)) {
              temp1.add(Integer.toString(RouletteNumber.DOUBLE_ZERO));
            }
            else if (number.contains("-")) {
              if (type == InsideBetType.STRAIGHT) {
                System.out.println(errorMsg);
                continue;
              }
              List<String> temp2 = Arrays.asList(number.split("-"));
              List<Integer> integers = new ArrayList<>();
              for (String t : temp2) {
                if ("00".equals(t)) {
                  integers.add(RouletteNumber.DOUBLE_ZERO);
                }
                else {
                  integers.add(Integer.parseInt(t));
                }
              }
              if (validateBets(integers)) {
                bets.add(new Bet(integers, type, wager));
              }
              else {
                System.out.println(errorMsg);
              }
            }
            else {
              if (type != InsideBetType.STRAIGHT) {
                System.out.println(errorMsg);
                continue;
              }
              int n = Integer.parseInt(number);
              if (n < 38) {
                temp1.add(number);
              }
            }
          }
          if (type == InsideBetType.TOP_LINE) {
            bets.add(RouletteUtils.createTopLineBet(wager));
          }
          else {
            numbers = new ArrayList<>(temp1);

            for (String number : numbers) {
              bets.add(new Bet(Arrays.asList(Integer.parseInt(number)), type, wager));
            }
          }
          isListModelUpdated = updateListModel(bets);
        }
        if (isListModelUpdated) {
          clearBetsMenuItem.setEnabled(true);
        }
      }

    });
  }

  /**
   * Returns true if the bets are valid, false otherwise.
   * 
   * @param integers The list of bets.
   * @return True if the bets are valid, false otherwise.
   */
  private boolean validateBets(List<Integer> integers) {
    if (integers == null || integers.isEmpty()) {
      throw new IllegalArgumentException("integers is null or empty.");
    }

    List<Integer> temp = new ArrayList<>(integers);
    Collections.sort(temp);

    String s = insideBetCombo.getSelectedItem().toString();
    InsideBetType type = InsideBetType.getEnumFromDisplayName(s);

    switch (type) {
    case SPLIT: {
      if (temp.size() != 2) {
        return false;
      }
      int first = temp.get(0);
      int last = temp.get(1);
      if (first % 3 == 0) {
        if (first + 1 == last) {
          return false;
        }
        if (first + 3 == last) {
          return true;
        }
      }
      return (first + 1 == last || first + 3 == last) && first >= 1 && last <= 36;
    }
    case STREET: {
      if (temp.size() != 3) {
        return false;
      }
      if (temp.get(2) % 3 != 0) {
        return false;
      }
      int first = temp.get(0);
      int last = temp.get(2);
      if (first + 1 == temp.get(1) && first + 2 == last) {
        return first >= 1 && last <= 36;
      }
      return false;
    }
    case CORNER: {
      if (temp.size() != 4) {
        return false;
      }
      int first = temp.get(0);
      int last = temp.get(3);
      if (first + 3 != temp.get(2) && first + 1 != last) {
        return false;
      }
      return first >= 1 && last <= 36;
    }
    case DOUBLE_STREET: {
      if (temp.size() != 6) {
        return false;
      }
      if (temp.get(2) % 3 != 0 && temp.get(5) % 3 != 0) {
        return false;
      }
      int first = temp.get(0);
      int last = temp.get(5);
      if (first + 1 == temp.get(1) && first + 2 == temp.get(2)) {
        first = temp.get(3);
        if (first + 1 == temp.get(4) && first + 2 == last) {
          return first >= 1 && last <= 36;
        }
      }
      return false;
    }
    case BASKET:
      if (temp.size() != 3) {
        return false;
      }
      if (temp.containsAll(Arrays.asList(0, RouletteNumber.DOUBLE_ZERO, 2))) {
        return true;
      }
      if (temp.containsAll(Arrays.asList(0, RouletteNumber.DOUBLE_ZERO, 3))) {
        return true;
      }
      if (temp.containsAll(Arrays.asList(0, 1, 2))) {
        return true;
      }
      return false;
    default:
      return false;
    }
  }

  /**
   * Returns a panel that contains a combo box containing a list of different types of inside bets.
   * 
   * @return A panel.
   */
  private JPanel createInsideBetPanel() {
    JPanel insideBetPanel = new JPanel(new MigLayout("", "0[100!]15[]0", "0[]0"));

    insideBetBtn.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        outsideBetCombo.setEnabled(false);
        insideBetCombo.setEnabled(true);
        betsTextField.setEnabled(true);
        addBetBtn.setEnabled(!betsTextField.getText().isEmpty()
            && !wagerTextField.getText().isEmpty());
      }

    });
    insideBetPanel.add(insideBetBtn);

    for (InsideBetType type : InsideBetType.values()) {
      insideBetCombo.addItem(type.getDisplayName());
    }
    if (insideBetCombo.getItemCount() > 0) {
      insideBetCombo.setSelectedIndex(0);
    }
    insideBetCombo.setEnabled(false);
    insideBetCombo.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
          String s = event.getItem().toString();
          InsideBetType type = InsideBetType.getEnumFromDisplayName(s);
          betsTextField.setEnabled(type != InsideBetType.TOP_LINE);
        }
      }

    });
    insideBetPanel.add(insideBetCombo);
    return insideBetPanel;
  }

  /**
   * Returns a panel that contains a combo box containing a list of different types of outside bets.
   * 
   * @return A panel.
   */
  private JPanel createOutsideBetPanel() {
    JPanel outsideBetPanel = new JPanel(new MigLayout("", "0[100!]15[]0", "0[]0"));

    outsideBetBtn.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        outsideBetCombo.setEnabled(true);
        insideBetCombo.setEnabled(false);
        betsTextField.setEnabled(false);
        addBetBtn.setEnabled(!wagerTextField.getText().isEmpty());
      }

    });
    outsideBetPanel.add(outsideBetBtn);

    for (OutsideBetType type : OutsideBetType.values()) {
      outsideBetCombo.addItem(type.getDisplayName());
    }
    if (outsideBetCombo.getItemCount() > 0) {
      outsideBetCombo.setSelectedIndex(0);
    }
    outsideBetCombo.setEnabled(false);
    outsideBetPanel.add(outsideBetCombo);
    return outsideBetPanel;
  }

  /**
   * Creates a Document for a text field that only accepts numeric input.
   * 
   * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
   */
  public static class NumberDocument extends PlainDocument {

    private static final long serialVersionUID = 1L;

    private final int limit;

    /**
     * Creates a new NumberDocument instance.
     * 
     * @param limit The max number of characters for a text field.
     */
    public NumberDocument(int limit) {
      this.limit = limit;
    }

    /** {@inheritDoc} */
    @Override
    public void insertString(int pos, String text, AttributeSet as) throws BadLocationException {
      try {
        Integer.parseInt(text);
        if ((getLength() + text.length()) <= limit) {
          super.insertString(pos, text, as);
        }
      }
      catch (NumberFormatException e) {
        Toolkit.getDefaultToolkit().beep();
      }
    }

  }

  /**
   * Creates a Document for a text field that only accepts numeric input.
   * 
   * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
   */
  public static class NumberDocument2 extends PlainDocument {

    private static final long serialVersionUID = 1L;

    /** {@inheritDoc} */
    @Override
    public void insertString(int pos, String text, AttributeSet as) throws BadLocationException {
      try {
        if (text.matches("^[0-9,-]+$")) {
          super.insertString(pos, text, as);
        }
      }
      catch (NumberFormatException e) {
        Toolkit.getDefaultToolkit().beep();
      }
    }

  }

  /**
   * Returns a panel that contains a text field in which the player can enter a wager for a bet.
   * 
   * @return A panel.
   */
  private JPanel createWagerPanel() {
    wagerTextField.setDocument(new NumberDocument(9));
    wagerTextField.addKeyListener(new KeyAdapter() {

      @Override
      public void keyReleased(KeyEvent event) {
        setAddBetButtonState();
      }

    });
    wagerTextField.setColumns(10);
    wagerTextField.setFont(FONT);

    JPanel wagerPanel = new JPanel(new MigLayout("", "0[][]0", "0[]0"));

    JLabel question = new JLabel("How much do you want to bet for each number?");
    question.setFont(FONT);
    question.setVisible(false);
    wagerPanel.add(question);

    JLabel dollarSign = new JLabel("$ ");
    dollarSign.setFont(FONT);
    wagerPanel.add(dollarSign);

    wagerPanel.add(wagerTextField, "grow");
    return wagerPanel;
  }

  /**
   * Sets the state of the Add Bet button.
   */
  private void setAddBetButtonState() {
    String wager = wagerTextField.getText();
    if (insideBetBtn.isSelected()) {
      String bets = betsTextField.getText();
      String s = insideBetCombo.getSelectedItem().toString();
      InsideBetType type = InsideBetType.getEnumFromDisplayName(s);
      if (!bets.isEmpty() || type == InsideBetType.TOP_LINE) {
        addBetBtn.setEnabled(!wager.isEmpty() && !wager.startsWith("0"));
      }
    }
    else {
      addBetBtn.setEnabled(!wager.isEmpty() && !wager.startsWith("0"));
    }
  }

  /**
   * Returns a scroll pane that will contain a list of bets that were added by the player.
   * 
   * @return A scroll pane.
   */
  private JScrollPane createBetsList() {
    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    list.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent event) {
        if (!SwingUtilities.isRightMouseButton(event)) {
          return;
        }
        if (list.getSelectedValue() == null) {
          return;
        }

        JPopupMenu menu = new JPopupMenu();
        JMenuItem removeSelectedMenuItem = new JMenuItem("Remove Selected Bet");
        removeSelectedMenuItem.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent event) {
            for (int index = 0; index < listModel.getSize(); index++) {
              if (listModel.getElementAt(index).equals(list.getSelectedValue())) {
                listModel.remove(index);
                if (listModel.isEmpty()) {
                  clearBetsMenuItem.setEnabled(false);
                }
                return;
              }
            }
          }

        });
        menu.add(removeSelectedMenuItem);
        menu.show(list, event.getX(), event.getY());
      }

    });
    list.setCellRenderer(new ListCellRenderer<Bet>() {

      @Override
      public Component getListCellRendererComponent(JList<? extends Bet> list, Bet bet, int index,
          boolean isSelected, boolean cellHasFocus) {
        JLabel label = new JLabel(" " + bet.getType().getDisplayName());
        String message = ": " + NumberFormat.getCurrencyInstance().format(bet.getWager());
        if (bet.getType() instanceof InsideBetType) {
          StringBuffer buffer = new StringBuffer(" [");
          for (RouletteNumber number : bet.getNumbers()) {
            String temp;
            if (number.getValue() == RouletteNumber.DOUBLE_ZERO) {
              temp = "00, ";
            }
            else {
              temp = number.getValue() + ", ";
            }
            buffer.append(temp);
          }
          message += buffer.toString().substring(0, buffer.toString().length() - 2) + "]";
        }
        label.setText(label.getText() + message);
        label.setFont(FONT);
        label.setOpaque(true);
        label.setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
        label.setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
        return label;
      }

    });
    return new JScrollPane(list);
  }

  /**
   * Returns a list of all bets that the player made.
   * 
   * @return A list of bets.
   */
  private List<Bet> getAllBets() {
    List<Bet> bets = new ArrayList<>();
    for (int index = 0; index < listModel.size(); index++) {
      bets.add(listModel.get(index));
    }
    return bets;
  }

  /**
   * This listener will update the enabled state of the Add Bet button.
   * 
   * @author BJ Peter DeLaCruz
   */
  private class TextFieldListener implements DocumentListener {

    /** {@inheritDoc} */
    @Override
    public void changedUpdate(DocumentEvent event) {
      updateAddBetBtnState();
    }

    /** {@inheritDoc} */
    @Override
    public void insertUpdate(DocumentEvent event) {
      updateAddBetBtnState();
    }

    /** {@inheritDoc} */
    @Override
    public void removeUpdate(DocumentEvent event) {
      updateAddBetBtnState();
    }

    /**
     * Updates the enabled state of the Run Simulation button.
     */
    private void updateAddBetBtnState() {
      addBetBtn.setEnabled(!betsTextField.getText().isEmpty()
          && !wagerTextField.getText().isEmpty());
    }
  }

}
