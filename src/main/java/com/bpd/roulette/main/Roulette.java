package com.bpd.roulette.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import com.bpd.roulette.Bet;
import com.bpd.roulette.RouletteNumber;
import com.bpd.roulette.RouletteSimulator;
import com.bpd.roulette.RouletteTable;
import com.bpd.roulette.RouletteWheel;
import com.bpd.roulette.simulators.JohnWayneStrategySimulator;
import com.bpd.roulette.simulators.RedSplitBlackSimulator;
import com.bpd.roulette.ui.CreateCustomStrategyDialog;
import com.bpd.roulette.ui.CreateCustomStrategyDialog.NumberDocument;
import com.bpd.utils.UiUtils;

/**
 * The main entry point of this application. A user can run simulations using a "canned" strategy,
 * e.g. John Wayne's strategy, or one that he or she created (see {@link CreateCustomStrategyDialog}
 * ).
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public class Roulette extends JPanel {

  private static final Logger LOGGER = Logger.getGlobal();

  private static final long serialVersionUID = 1L;

  private final JTextArea textArea;

  private List<Bet> bets = new ArrayList<>();
  private final JButton runButton;
  private final JRadioButton selectStrategyRadioBtn;
  private final JComboBox<String> simulationsCombo;
  private final JRadioButton customStrategyRadioBtn;

  private JMenuItem saveMenuItem;
  private JTextField timesField, bankrollField;

  /**
   * Creates the UI for a roulette simulation.
   */
  public Roulette() {
    if (System.getProperty("os.name").startsWith("Windows")) {
      setPreferredSize(new Dimension(1000, 500));
    }
    else {
      setPreferredSize(new Dimension(1100, 500));
    }
    setLayout(new BorderLayout());

    textArea = new JTextArea();
    textArea.setEditable(false);
    textArea.setMargin(new Insets(10, 10, 10, 10));
    Font font = new Font("Verdana", Font.PLAIN, 12);
    textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
    JScrollPane scrollPane = new JScrollPane(textArea);
    add(scrollPane, BorderLayout.CENTER);

    textArea.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent event) {
        if (!SwingUtilities.isRightMouseButton(event)) {
          return;
        }

        if (textArea.getText() == null || textArea.getText().isEmpty()) {
          return;
        }

        JPopupMenu menu = new JPopupMenu();
        saveMenuItem = new JMenuItem("Save Results...");

        setupSaveMenuItem();

        menu.add(saveMenuItem);
        menu.show(textArea, event.getX(), event.getY());
      }

    });

    JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

    JLabel timesLabel = new JLabel("No. iterations:");
    timesLabel.setFont(font);
    timesField = new JTextField();
    timesField.setColumns(5);
    timesField.setFont(font);
    timesField.setDocument(new NumberDocument(5));
    timesField.setText("100");
    timesField.getDocument().addDocumentListener(new TextFieldListener());

    final JButton customStrategyButton = new JButton("Custom...");
    simulationsCombo = new JComboBox<>();

    runButton = new JButton("Run Simulation");

    selectStrategyRadioBtn = new JRadioButton("Select a strategy:");
    selectStrategyRadioBtn.setFont(font);
    selectStrategyRadioBtn.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        customStrategyButton.setEnabled(false);
        simulationsCombo.setEnabled(true);
        runButton.setEnabled(isFieldsValid());
      }

    });

    simulationsCombo.setFont(font);
    simulationsCombo.addItem("John Wayne's Strategy");
    simulationsCombo.addItem("Red-Split-Black");
    simulationsCombo.setSelectedIndex(0);
    simulationsCombo.setEnabled(false);

    customStrategyRadioBtn = new JRadioButton("Create your own:");

    runButton.setFont(font);
    runButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        startSimulation();
      }

    });

    customStrategyRadioBtn.setFont(font);
    customStrategyRadioBtn.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        customStrategyButton.setEnabled(true);
        simulationsCombo.setEnabled(false);
        runButton.setEnabled(isFieldsValid() && !bets.isEmpty());
      }

    });

    customStrategyButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        CreateCustomStrategyDialog frame = new CreateCustomStrategyDialog(Roulette.this, bets);
        frame.pack();
        frame.setLocationRelativeTo(Roulette.this);
        frame.setVisible(true);
      }

    });
    customStrategyButton.setFont(font);
    customStrategyButton.setEnabled(false);

    ButtonGroup group = new ButtonGroup();
    group.add(customStrategyRadioBtn);
    group.add(selectStrategyRadioBtn);

    southPanel.add(timesLabel);
    southPanel.add(timesField);

    southPanel.add(createEmptyPanel());

    JLabel dollarSymbol = new JLabel();
    dollarSymbol.setText("$");
    dollarSymbol.setFont(font);
    southPanel.add(dollarSymbol);

    bankrollField = new JTextField();
    bankrollField.setColumns(5);
    bankrollField.setFont(font);
    bankrollField.setDocument(new NumberDocument(5));
    bankrollField.setText("500");
    bankrollField.getDocument().addDocumentListener(new TextFieldListener());
    southPanel.add(bankrollField);

    southPanel.add(createEmptyPanel());
    southPanel.add(selectStrategyRadioBtn);
    southPanel.add(simulationsCombo);

    southPanel.add(createEmptyPanel());
    southPanel.add(customStrategyRadioBtn);
    southPanel.add(customStrategyButton);

    southPanel.add(createEmptyPanel());
    southPanel.add(runButton);

    add(southPanel, BorderLayout.SOUTH);
    selectStrategyRadioBtn.doClick();
  }

  /**
   * Returns true if the fields for number of iterations and bankroll are valid.
   * 
   * @return True if "1" or more is entered for number of iterations and bankroll, false if "0" or
   * empty string.
   */
  private boolean isFieldsValid() {
    String numberIterations = timesField.getText();
    if (!numberIterations.isEmpty() && !"0".equals(numberIterations)) {
      String bankroll = bankrollField.getText();
      return !bankroll.isEmpty() && !"0".equals(bankroll);
    }
    return false;
  }

  /**
   * Starts the roulette simulation, which is run on a separate thread.
   */
  private void startSimulation() {
    Thread t = new Thread() {

      @Override
      public void run() {
        int times = Integer.parseInt(timesField.getText());

        if (customStrategyRadioBtn.isSelected()) {
          runCustomSimulation(times);
          return;
        }

        final RouletteSimulator simulator;
        int index = simulationsCombo.getSelectedIndex();
        switch (index) {
        case 0:
          simulator = new JohnWayneStrategySimulator(times);
          break;
        case 1:
          simulator = new RedSplitBlackSimulator(times);
          break;
        default:
          throw new IllegalArgumentException("Invalid index: " + index);
        }
        simulator.run();
        SwingUtilities.invokeLater(new Runnable() {

          @Override
          public void run() {
            textArea.setText(simulator.getResults());
          }

        });
      }

    };
    t.start();
  }

  /**
   * Sets up the Save Results menu item (adds an action listener, etc.).
   */
  private void setupSaveMenuItem() {
    saveMenuItem.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        JFileChooser chooser = new JFileChooser();
        chooser.setSelectedFile(new File("results.txt"));
        FileFilter filter = new FileNameExtensionFilter("Text file", "txt");
        chooser.setFileFilter(filter);
        int option = chooser.showSaveDialog(Roulette.this);
        if (option != JFileChooser.APPROVE_OPTION) {
          return;
        }
        File file = chooser.getSelectedFile();
        if (file.exists()) {
          String msg = file.getName() + " already exists. Overwrite?";
          int result =
              JOptionPane.showConfirmDialog(Roulette.this, msg, "Overwrite Existing File",
                  JOptionPane.YES_NO_CANCEL_OPTION);
          switch (result) {
          case JOptionPane.YES_OPTION:
            break;
          case JOptionPane.NO_OPTION:
            // fall-through
          case JOptionPane.CANCEL_OPTION:
            // fall-through
          case JOptionPane.CLOSED_OPTION:
            return;
          default:
            throw new RuntimeException("Unsupported case: " + result);
          }
        }
        try {
          String text;
          if (selectStrategyRadioBtn.isSelected()) {
            text = simulationsCombo.getSelectedItem().toString() + "\n\n";
          }
          else {
            StringBuffer buffer = new StringBuffer();
            for (Bet bet : bets) {
              String wager = NumberFormat.getCurrencyInstance().format(bet.getWager());
              StringBuffer buff = new StringBuffer();
              buff.append("[");
              for (RouletteNumber number : bet.getNumbers()) {
                buff.append(number.getValue() + ", ");
              }
              String numbers = buff.toString().substring(0, buff.toString().length() - 2) + "]";
              String type = bet.getType().getDisplayName();
              buffer.append(wager + " - " + numbers + " (" + type + ")\n");
            }
            text = buffer.toString() + "\n";
          }
          text += textArea.getText();
          Files.write(chooser.getSelectedFile().toPath(), text.getBytes("UTF-16"));
        }
        catch (IOException e) {
          JOptionPane.showMessageDialog(Roulette.this, "Unable to save results to file.", "Error",
              JOptionPane.ERROR_MESSAGE);
        }
      }

    });
  }

  /**
   * Enables or disables the Run Simulation button.
   * 
   * @param flag True to enable the button, false to disable it.
   */
  public void enableRunButton(boolean flag) {
    runButton.setEnabled(flag);
  }

  /**
   * Sets the list to the given bets.
   * 
   * @param bets The list of bets.
   */
  public void setBets(List<Bet> bets) {
    if (bets == null) {
      throw new IllegalArgumentException("bets is null");
    }
    this.bets.clear();
    this.bets.addAll(bets);
  }

  /**
   * Runs the betting simulation that the user created a given number of times.
   * 
   * @param times The number of times to run the user's betting simulation.
   */
  private void runCustomSimulation(final int times) {
    if (times < 1) {
      throw new IllegalArgumentException("times must be 1 or greater");
    }

    Thread t = new Thread() {

      @Override
      public void run() {
        final StringBuffer buffer = new StringBuffer(100);
        RouletteWheel wheel = new RouletteWheel();
        int amount = Integer.parseInt(bankrollField.getText());
        RouletteTable table = new RouletteTable(amount);
        for (int i = 0; i < times; i++) {
          try {
            table.placeBets(bets.toArray(new Bet[bets.size()]));
          }
          catch (IllegalStateException e) {
            String msg = e.getMessage() + "\n";
            buffer.append(msg);
            break;
          }
          wheel.spinWheel();
          RouletteNumber number = wheel.getWinningNumber();
          table.checkBets(number);
          buffer.append(getLineNumber(times, i + 1) + number.getDisplayString());
          String total = NumberFormat.getCurrencyInstance().format(table.getBankroll());
          if (table.getBankroll() < amount) {
            String temp = NumberFormat.getCurrencyInstance().format(amount - table.getBankroll());
            total += "  (-" + temp + ")";
          }
          buffer.append("\t|   " + total + "\n");
          table.clearBets();
          if (table.getBankroll() == 0) {
            buffer.append("No more money left.");
            break;
          }
        }
        SwingUtilities.invokeLater(new Runnable() {

          @Override
          public void run() {
            textArea.setText(buffer.toString());
          }

        });
      }

    };
    t.start();

  }

  /**
   * Returns an empty panel that is 10 pixels wide.
   * 
   * @return An empty panel.
   */
  private JPanel createEmptyPanel() {
    JPanel emptyPanel = new JPanel();
    emptyPanel.setPreferredSize(new Dimension(10, simulationsCombo.getPreferredSize().height));
    return emptyPanel;
  }

  /**
   * Prints the current line number to a string. Pads the end of the string with spaces if
   * necessary.
   * 
   * @param numLines The total number of lines.
   * @param currentLine The current line number.
   * @return The current line number with or without spaces at the end of the string.
   */
  public static String getLineNumber(int numLines, int currentLine) {
    int totalLength = Integer.toString(numLines).length();
    int currentLength = Integer.toString(currentLine).length();
    StringBuffer buff = new StringBuffer(totalLength);
    buff.append(currentLine);
    for (int j = 0; j < totalLength - currentLength; j++) {
      buff.append(" ");
    }
    buff.append("   |   ");
    return buff.toString();
  }

  /**
   * This listener will update the enabled state of the Run Simulation button.
   * 
   * @author BJ Peter DeLaCruz
   */
  private class TextFieldListener implements DocumentListener {

    /** {@inheritDoc} */
    @Override
    public void changedUpdate(DocumentEvent event) {
      updateRunSimulationBtnState();
    }

    /** {@inheritDoc} */
    @Override
    public void insertUpdate(DocumentEvent event) {
      updateRunSimulationBtnState();
    }

    /** {@inheritDoc} */
    @Override
    public void removeUpdate(DocumentEvent event) {
      updateRunSimulationBtnState();
    }

    /**
     * Updates the enabled state of the Run Simulation button.
     */
    private void updateRunSimulationBtnState() {
      String numberIterations = timesField.getText();
      if (!numberIterations.isEmpty() && !numberIterations.startsWith("0")) {
        String bankroll = bankrollField.getText();
        if (!bankroll.isEmpty() && !bankroll.startsWith("0")) {
          if (selectStrategyRadioBtn.isSelected()) {
            runButton.setEnabled(true);
          }
          else {
            runButton.setEnabled(!bets.isEmpty());
          }
        }
        else {
          runButton.setEnabled(false);
        }
      }
      else {
        runButton.setEnabled(false);
      }
    }
  }

  /**
   * Main entry point of the application.
   * 
   * @param args None.
   */
  public static void main(String... args) {
    UiUtils.useSystemLookAndFeel(LOGGER);
    JFrame frame = new JFrame();
    URL iconUrl = CreateCustomStrategyDialog.class.getResource("images/roulette.png");
    if (iconUrl != null) {
      frame.setIconImage(new ImageIcon(iconUrl).getImage());
    }
    UiUtils.setEscKey(frame);
    frame.setTitle("Roulette Simulator");
    frame.getContentPane().add(new Roulette());
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setResizable(false);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }

}
