package com.bpd.roulette;

import static com.bpd.roulette.RouletteNumber.createNumbers;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents a bet on a roulette table.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public final class Bet {

  private final List<RouletteNumber> numbers;
  private final BetType type;
  private final int wager;

  /**
   * Creates a new Bet instance.
   * 
   * @param type The type of bet.
   * @param wager The wager in U.S. dollars.
   */
  Bet(BetType type, int wager) {
    if (type == null) {
      throw new IllegalArgumentException("type is null.");
    }
    if (wager < 0) {
      throw new IllegalArgumentException("wager cannot be negative.");
    }
    this.numbers = null;
    this.type = type;
    this.wager = wager;
  }

  /**
   * Creates a new Bet instance.
   * 
   * @param numbers The list of numbers on which to bet.
   * @param type The type of bet.
   * @param wager The wager in U.S. dollars.
   */
  public Bet(List<Integer> numbers, BetType type, int wager) {
    if (numbers == null) {
      throw new IllegalArgumentException("numbers is null.");
    }
    if (type == null) {
      throw new IllegalArgumentException("type is null.");
    }
    if (wager < 0) {
      throw new IllegalArgumentException("wager cannot be negative.");
    }
    this.numbers = createNumbers(numbers);
    this.type = type;
    this.wager = wager;
  }

  /** @return The list of numbers on which to bet. */
  public List<RouletteNumber> getNumbers() {
    return new ArrayList<>(numbers);
  }

  /** @return The type of bet. */
  public BetType getType() {
    return type;
  }

  /** @return The wager in U.S. dollars. */
  public int getWager() {
    return wager;
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof Bet)) {
      return false;
    }
    Bet bet = (Bet) object;
    return bet.numbers.equals(numbers) && bet.type.equals(type)
        && Double.compare(bet.wager, wager) == 0;
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return Objects.hash(numbers, type, wager);
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return "Bet[numbers=" + numbers + ", type=" + type + "]";
  }

}
