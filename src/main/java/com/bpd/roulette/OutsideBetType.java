package com.bpd.roulette;

/**
 * Represents the different types of outside bets.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public enum OutsideBetType implements BetType {

  /** 1-12 bet. */
  FIRST_DOZEN(2, "1 to 12"),
  /** 13-24 bet. */
  SECOND_DOZEN(2, "13 to 24"),
  /** 25-36 bet. */
  THIRD_DOZEN(2, "25 to 36"),
  /** 1st column bet. */
  FIRST_COLUMN(2, "1st Column"),
  /** 2nd column bet. */
  SECOND_COLUMN(2, "2nd Column"),
  /** 3rd column bet. */
  THIRD_COLUMN(2, "3rd Column"),
  /** 1-18 bet. */
  FIRST_EIGHTEEN(1, "1 to 18"),
  /** 19-36 bet. */
  LAST_EIGHTEEN(1, "18 to 36"),
  /** Even bet. */
  EVEN(1, "Even"),
  /** Odd bet. */
  ODD(1, "Odd"),
  /** Black bet. */
  BLACK(1, "Black"),
  /** Red bet. */
  RED(1, "Red");

  private final int payout;
  private final String displayName;

  /**
   * Creates a new OutsideBetType enum.
   * 
   * @param payout The payout for the given outside bet.
   * @param displayName The display name for this enum.
   */
  OutsideBetType(int payout, String displayName) {
    this.payout = payout;
    this.displayName = displayName;
  }

  /** {@inheritDoc} */
  @Override
  public int getPayout() {
    return payout;
  }

  /** {@inheritDoc} */
  @Override
  public String getDisplayName() {
    return displayName;
  }

  /**
   * Returns an OutsideBetType enum with the given display name. Throws an exception if no enum
   * exists.
   * 
   * @param displayName The display name.
   * @return An OutsideBetType enum with the given display name.
   */
  public static OutsideBetType getEnumFromDisplayName(String displayName) {
    for (OutsideBetType type : OutsideBetType.values()) {
      if (type.displayName.equals(displayName)) {
        return type;
      }
    }
    throw new IllegalArgumentException("Invalid display name: " + displayName);
  }

}
