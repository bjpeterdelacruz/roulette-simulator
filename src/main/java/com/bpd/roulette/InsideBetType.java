package com.bpd.roulette;

/**
 * Represents the different types of inside bets.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public enum InsideBetType implements BetType {

  /** Straight bet. */
  STRAIGHT(35, "Single"),
  /** Split bet. */
  SPLIT(17, "Split"),
  /** Street bet. */
  STREET(11, "Street"),
  /** Corner bet. */
  CORNER(8, "Corner"),
  /** Double street bet. */
  DOUBLE_STREET(5, "Double Street"),
  /** Basket bet. */
  BASKET(11, "Basket"),
  /** Top line bet. */
  TOP_LINE(6, "Top Line");

  private final int payout;
  private final String displayName;

  /**
   * Creates a new InsideBetType enum.
   * 
   * @param payout The payout for the given inside bet.
   * @param displayName The display name for this enum.
   */
  InsideBetType(int payout, String displayName) {
    this.payout = payout;
    this.displayName = displayName;
  }

  /** {@inheritDoc} */
  @Override
  public int getPayout() {
    return payout;
  }

  /** {@inheritDoc} */
  @Override
  public String getDisplayName() {
    return displayName;
  }

  /**
   * Returns an InsideBetType enum with the given display name. Throws an exception if no enum
   * exists.
   * 
   * @param displayName The display name.
   * @return An InsideBetType enum with the given display name.
   */
  public static InsideBetType getEnumFromDisplayName(String displayName) {
    for (InsideBetType type : InsideBetType.values()) {
      if (type.displayName.equals(displayName)) {
        return type;
      }
    }
    throw new IllegalArgumentException("Invalid display name: " + displayName);
  }
}
