package com.bpd.roulette;

/**
 * Represents the type of bet to make on a roulette table.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public interface BetType {

  /**
   * The payout. For example, a split inside bet pays 17:1 where 17 is the payout.
   * 
   * @return The payout.
   */
  int getPayout();

  /**
   * Returns the display name for this enum.
   * 
   * @return The display name.
   */
  String getDisplayName();

}
