package com.bpd.roulette;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static com.bpd.roulette.RouletteNumber.createNumber;

/**
 * Represents a roulette wheel.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public final class RouletteWheel {

  private final Random random;
  private final List<RouletteNumber> history;

  /**
   * Creates a new RouletteWheel.
   */
  public RouletteWheel() {
    random = new Random();
    history = new ArrayList<>();
  }

  /** Spins the wheel and adds the winning number to the history. */
  public void spinWheel() {
    history.add(createNumber(random.nextInt(38)));
  }

  /** @return The winning number (the number on which the ball landed). */
  public RouletteNumber getWinningNumber() {
    if (history.isEmpty()) {
      throw new IllegalArgumentException("Wheel hasn't spun yet.");
    }
    return history.get(history.size() - 1);
  }

  /** @return The history, which contains all winning numbers. */
  public List<RouletteNumber> getHistory() {
    return new ArrayList<>(history);
  }

}
