package com.bpd.roulette;

import java.util.Arrays;
import java.util.List;

/**
 * This class contains utility methods such as those that create different types of bets.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public final class RouletteUtils {

  /**
   * Creates a new instance of RouletteUtils.
   */
  private RouletteUtils() {
  }

  /**
   * Creates a $1 bet on an inside bet.
   * 
   * @param numbers The list of numbers on which to bet.
   * @param type The type of inside bet.
   * @return A $1 bet.
   */
  public static Bet createDollarBet(List<Integer> numbers, InsideBetType type) {
    return new Bet(numbers, type, 1);
  }

  /**
   * Creates a $5 bet on an inside bet.
   * 
   * @param numbers The list of numbers on which to bet.
   * @param type The type of inside bet.
   * @return A $5 bet.
   */
  public static Bet createFiveDollarBet(List<Integer> numbers, InsideBetType type) {
    return new Bet(numbers, type, 5);
  }

  /**
   * Creates a $1 bet on an outside bet.
   * 
   * @param type The type of outside bet.
   * @return A $1 bet.
   */
  public static Bet createDollarBet(OutsideBetType type) {
    return new Bet(type, 1);
  }

  /**
   * Creates a $5 bet on an outside bet.
   * 
   * @param type The type of outside bet.
   * @return A $5 bet.
   */
  public static Bet createFiveDollarBet(OutsideBetType type) {
    return new Bet(type, 5);
  }

  /**
   * Creates an outside bet with the given wager.
   * 
   * @param wager The wager.
   * @param type The type of outside bet.
   * @return An outside bet.
   */
  public static Bet createOutsideBet(int wager, OutsideBetType type) {
    return new Bet(type, wager);
  }

  /**
   * Creates a top-line bet with the given wager.
   * 
   * @param wager The wager for this top-line bet.
   * @return A new top-line bet.
   */
  public static Bet createTopLineBet(int wager) {
    if (wager < 0) {
      throw new IllegalArgumentException("wager must be 0 or greater.");
    }
    return new Bet(Arrays.asList(0, RouletteNumber.DOUBLE_ZERO, 1, 2, 3), InsideBetType.TOP_LINE,
        wager);
  }

}
