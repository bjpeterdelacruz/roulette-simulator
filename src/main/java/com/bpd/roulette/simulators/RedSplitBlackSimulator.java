package com.bpd.roulette.simulators;

import java.text.NumberFormat;
import java.util.Arrays;
import com.bpd.roulette.InsideBetType;
import com.bpd.roulette.OutsideBetType;
import com.bpd.roulette.RouletteNumber;
import com.bpd.roulette.RouletteSimulator;
import com.bpd.roulette.RouletteTable;
import com.bpd.roulette.RouletteUtils;
import com.bpd.roulette.RouletteWheel;
import com.bpd.roulette.main.Roulette;

/**
 * Implements the red-split-black roulette betting strategy. This strategy involves betting $7 on
 * red and $1 on all adjoining black numbers, i.e. split bets (there are five total). Only $12 are
 * on the table at any given time, and the chances of losing $12 are about <span
 * style="font-weight: bold; color: red">26%</span>. If the ball lands on a red number, <span
 * style="font-weight: bold; color: green">$2 profit</span>. If the ball lands on a split bet, <span
 * style="font-weight: bold; color: green">$6 profit</span>.
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public class RedSplitBlackSimulator implements RouletteSimulator {

  private String results;
  private final int times;

  /**
   * Creates a new RedSplitBlackSimulator with the given number of times to run the simulation.
   * 
   * @param times The number of times to run the simulation.
   */
  public RedSplitBlackSimulator(int times) {
    this.times = times;
  }

  /** {@inheritDoc} */
  @Override
  public void run() {
    StringBuffer buffer = new StringBuffer(100);
    RouletteWheel wheel = new RouletteWheel();
    int amount = 500;
    RouletteTable table = new RouletteTable(amount);
    for (int i = 0; i < times; i++) {
      try {
        table.placeBets(RouletteUtils.createOutsideBet(7, OutsideBetType.RED));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(8, 11), InsideBetType.SPLIT));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(17, 20), InsideBetType.SPLIT));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(26, 29), InsideBetType.SPLIT));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(10, 13), InsideBetType.SPLIT));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(28, 31), InsideBetType.SPLIT));
      }
      catch (IllegalStateException e) {
        String msg = e.getMessage() + "\n";
        buffer.append(msg);
        break;
      }
      wheel.spinWheel();
      RouletteNumber number = wheel.getWinningNumber();
      table.checkBets(number);
      buffer.append(Roulette.getLineNumber(times, i + 1) + number.getDisplayString());
      String total = NumberFormat.getCurrencyInstance().format(table.getBankroll());
      if (table.getBankroll() < amount) {
        String temp = NumberFormat.getCurrencyInstance().format(amount - table.getBankroll());
        total += "  (-" + temp + ")";
      }
      buffer.append("\t|   " + total + "\n");
      table.clearBets();
      if (table.getBankroll() == 0) {
        buffer.append("No more money left.");
        break;
      }
    }
    results = buffer.toString();
  }

  /** {@inheritDoc} */
  @Override
  public String getResults() {
    if (results == null) {
      throw new IllegalArgumentException("Simulation hasn't run yet.");
    }
    return results;
  }

  /** {@inheritDoc} */
  @Override
  public String getName() {
    return "Red-Split Black Strategy";
  }

}
