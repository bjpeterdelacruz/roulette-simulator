package com.bpd.roulette.simulators;

import java.text.NumberFormat;
import java.util.Arrays;
import com.bpd.roulette.InsideBetType;
import com.bpd.roulette.RouletteNumber;
import com.bpd.roulette.RouletteSimulator;
import com.bpd.roulette.RouletteTable;
import com.bpd.roulette.RouletteUtils;
import com.bpd.roulette.RouletteWheel;
import com.bpd.roulette.main.Roulette;

/**
 * Implements John's Wayne's betting strategy. This strategy involves putting $1 on one number and
 * $1 on another number (two straight bets), and then making four corner bets for each straight bet
 * ($1 for each corner bet). For example, if the first straight bet is 8, make the following corner
 * bets:
 * <ul>
 * <li>4-5-7-8</li>
 * <li>5-6-8-9
 * <li></li>7-8-10-11</li>
 * <li>8-9-11-12</li>
 * </ul>
 * Notice that 8 is in each corner bet.</p>
 * 
 * The chances of losing $10 using this strategy are about <span
 * style="font-weight: bold; color: red">56%</span>. If the ball lands on any of the straight bets,
 * <span style="font-weight: bold; color: green">$67 profit</span>. If the ball lands between two
 * corner bets, <span style="font-weight: bold; color: green">$10 profit</span>. If the ball lands
 * on only one corner bet, <span style="font-weight: bold; color: red">$1 loss</span>.</p>
 * 
 * @author BJ Peter DeLaCruz <bjpeter@hawaii.edu>
 */
public class JohnWayneStrategySimulator implements RouletteSimulator {

  private String results;
  private final int times;

  /**
   * Creates a new JohnWayneStrategySimulator with the given number of times to run the simulation.
   * 
   * @param times The number of times to run the simulation.
   */
  public JohnWayneStrategySimulator(int times) {
    this.times = times;
  }

  /** {@inheritDoc} */
  @Override
  public void run() {
    StringBuffer buffer = new StringBuffer(100);
    RouletteWheel wheel = new RouletteWheel();
    int amount = 500;
    RouletteTable table = new RouletteTable(amount);
    for (int i = 0; i < times; i++) {
      try {
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(8), InsideBetType.STRAIGHT));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(4, 5, 7, 8),
            InsideBetType.CORNER));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(5, 6, 8, 9),
            InsideBetType.CORNER));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(7, 8, 10, 11),
            InsideBetType.CORNER));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(8, 9, 11, 12),
            InsideBetType.CORNER));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(20), InsideBetType.STRAIGHT));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(16, 17, 19, 20),
            InsideBetType.CORNER));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(17, 18, 20, 21),
            InsideBetType.CORNER));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(19, 20, 22, 23),
            InsideBetType.CORNER));
        table.placeBets(RouletteUtils.createDollarBet(Arrays.asList(20, 21, 23, 24),
            InsideBetType.CORNER));
      }
      catch (IllegalStateException e) {
        String msg = e.getMessage() + "\n";
        buffer.append(msg);
        break;
      }
      wheel.spinWheel();
      RouletteNumber number = wheel.getWinningNumber();
      table.checkBets(number);
      buffer.append(Roulette.getLineNumber(times, i + 1) + number.getDisplayString());
      String total = NumberFormat.getCurrencyInstance().format(table.getBankroll());
      if (table.getBankroll() < amount) {
        String temp = NumberFormat.getCurrencyInstance().format(amount - table.getBankroll());
        total += "  (-" + temp + ")";
      }
      buffer.append("\t|   " + total + "\n");
      table.clearBets();
      if (table.getBankroll() == 0) {
        buffer.append("No more money left.");
        break;
      }
    }
    results = buffer.toString();
  }

  /** {@inheritDoc} */
  @Override
  public String getResults() {
    if (results == null) {
      throw new IllegalArgumentException("Simulation hasn't run yet.");
    }
    return results;
  }

  /** {@inheritDoc} */
  @Override
  public String getName() {
    return "John Wayne's Strategy";
  }

}
